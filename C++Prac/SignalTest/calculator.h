#include <boost/signals2.hpp>
#include "functions.h"

typedef boost::signals2::signal<void (float, float)> FunctionSignal;
typedef FunctionSignal::slot_type OnFunction_slot;
typedef boost::signals2::connection connection_m;

class Calculator
{
public:

void Process(float, float);
connection_m SetFunctionSignal(const OnFunction_slot& slot);
FunctionSignal m_functionSignal;

private:
int main();
};

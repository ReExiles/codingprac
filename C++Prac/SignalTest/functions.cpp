#include "functions.h"

void
Functions::AddFunc(float entry1, float entry2){
    printf("Add Result: %0.2f\n\r", entry1 + entry2);
    return;
}

void
Functions::SubsFunc(float entry1, float entry2){
    printf("Substract Result: %0.2f\n\r", entry1 - entry2);
    return;
}

void
Functions::ProdFunc(float entry1, float entry2){
    printf("Product Result: %0.2f\n\r", entry1 * entry2);
    return;
}

void
Functions::DivFunc(float entry1, float entry2){
    printf("Division Result: %0.2f\n\r", entry1 / entry2);
    return;
}
#include "calculator.h"
#include <iostream>

using namespace std;

connection_m
Calculator::SetFunctionSignal(const OnFunction_slot& slot)
{
    return m_functionSignal.connect(slot);
}

void
Calculator::Process(float entry1, float entry2)
{
    m_functionSignal(entry1, entry2);
}

int main()
{
    char calc;
	float entry1, entry2;
	cout << "Enter two floating-point values: ";
	cin >> entry1;
	cin >> entry2;

    boost::shared_ptr<Functions> testFunc(new Functions());
    boost::shared_ptr<Calculator> calculator(new Calculator());
	
   	while(calc != 'o'){
		cout << "Enter an option (s: sum, r: subs, p: prod, d: div), when finish enter (o: ok): ";
		cin >> calc;
		switch (calc)
		{
			case 's':
    			calculator->SetFunctionSignal(OnFunction_slot(&Functions::AddFunc, testFunc.get(), _1, _2).track_foreign(testFunc));
				break;
			case 'r':
    			calculator->SetFunctionSignal(OnFunction_slot(&Functions::SubsFunc, testFunc.get(), _1, _2).track_foreign(testFunc));
				break;
			case 'p':
				calculator->SetFunctionSignal(OnFunction_slot(&Functions::ProdFunc, testFunc.get(), _1, _2).track_foreign(testFunc));
				break;
			case 'd':
				calculator->SetFunctionSignal(OnFunction_slot(&Functions::DivFunc, testFunc.get(), _1, _2).track_foreign(testFunc));
				break;


		}
	}	

    calculator->Process(entry1, entry2);
    printf("finish calculations\r\n");

}

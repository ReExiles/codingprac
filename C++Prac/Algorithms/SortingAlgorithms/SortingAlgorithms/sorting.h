#pragma once
#include <string>
#include <list>
#include <iostream>

using namespace std;

class ISorting {
protected:
	string name;
	int size;
	int* objList;
public:
	string GetSortigName() { return name; }
	
	virtual void PrintList() = 0;

	virtual void Sort() = 0;
};
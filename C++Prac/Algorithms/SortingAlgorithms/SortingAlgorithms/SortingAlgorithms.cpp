// SortingAlgorithms.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "QuickSort.h"

void SortArray(ISorting& obj, int arr[], int size)
{
	obj.Sort();
	obj.PrintList();
}

int main()
{
    std::cout << "Hello World!\n";
	int arr[] = { 10, 7, 8, 9, 1, 5, 0, 4, 1, 18, 3, 2 };
	int n = sizeof(arr) / sizeof(arr[0]);

	QuickSort objSort(n, arr);
	SortArray(objSort, arr, n);
}


// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file

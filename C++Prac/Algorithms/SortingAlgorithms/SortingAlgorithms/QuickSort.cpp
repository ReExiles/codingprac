#include "QuickSort.h"

QuickSort::QuickSort(int sizeInit, int listinit[])
{
	name = "Quick Sort";
	size = sizeInit;
	objList = listinit;
}

/*
arr[] --> Array to be sorted,
low --> Starting index,
high --> Ending index
*/
void QuickSort::SortRequest(int arr[], int low, int high)
{
	if (low < high)
	{
		/* pi is partitioning index, arr[p] is now
		at right place */
		int pi = Partition(arr, low, high);

		// Separately sort elements before  
		// partition and after partition  
		SortRequest(arr, low, pi - 1);
		SortRequest(arr, pi + 1, high);
	}
}

/* 
Select last element from array as the "pivot".
PLace smaller elements than pivot to the left.
PLace smaller elements than pivot to the right.
PLace pivot in the correct position of the sorted array.
*/
int QuickSort::Partition(int arr[], int low, int high)
{
	int pivot = arr[high]; // pivot  
	int i = (low - 1); // Index of smaller element 

	for (int j = low; j <= high - 1; ++j)
	{
		// If current element is smaller than the pivot  
		if (arr[j] < pivot)
		{
			i++; // increment index of smaller element  
			Swap(&arr[i], &arr[j]);
		}
	}
	Swap(&arr[i + 1], &arr[high]);
	return (i + 1);
}

void QuickSort::Sort()
{
	SortRequest(objList, 0, size - 1);
}

void QuickSort::Swap(int* a, int* b)
{
	int t = *a;
	*a = *b;
	*b = t;
}

void QuickSort::PrintList()
{
	printf("Using Algorithm (%s)\n\r", name.c_str());
	for (int i = 0; i < size; ++i) {
		std::cout << objList[i] << " ";
	}
}
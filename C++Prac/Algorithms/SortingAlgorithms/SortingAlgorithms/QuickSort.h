#pragma once

#include "sorting.h"

class QuickSort : public ISorting {
public:
	QuickSort(int size, int unList[]);
	void Sort();
	void PrintList();

private:
	string name;

private:
	void Swap(int* a, int* b);
	int Partition(int arr[], int low, int high);
	void SortRequest(int arr[], int low, int high);
};